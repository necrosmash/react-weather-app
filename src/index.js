import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import WeatherApiCallContainer from './components/weatherApiCallContainer';

ReactDOM.render(
    <WeatherApiCallContainer />,
    document.getElementById('root'));
