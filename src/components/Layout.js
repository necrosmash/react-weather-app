import React from 'react';
import Media from 'react-media';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import './layout.css';
import Home from './Home.js';
import DetailedWeatherInfo from './DetailedWeatherInfo.js';

const Layout = ({ latestWeather, weatherForecast }) => (
    <Media query={"(max-width: 799px)"}>
        {screenIsSmall =>
            screenIsSmall ? (
                <Router>
                    <Switch>
                        <div className="layout layout-small">
                            <Route exact path="/" render={(props) =>
                                <Home {...props} latestWeather={latestWeather} weatherForecast={weatherForecast} />
                            } />
                            <Route path="/detailedweatherinfo" component={DetailedWeatherInfo} />
                        </div>
                    </Switch>
                </Router>
            ) : (
                <Router>
                    <div className="layout layout-large">
                        <Route path="/" render={(props) =>
                            <Home {...props} latestWeather={latestWeather} weatherForecast={weatherForecast} />
                        } />
                        <Route path="/detailedweatherinfo" render={(props) => 
                            <DetailedWeatherInfo {...props}>
                                <Link className="close" to={{pathname: "/"}}>&nbsp;X&nbsp;</Link>
                            </DetailedWeatherInfo> 
                        } />
                    </div>
                </Router>
            )
        }
    </Media>
);

export default Layout;