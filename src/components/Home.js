import React from 'react';
import './Home.css';
import WeatherTile from './weatherTile.js';

const Home = ({ latestWeather, weatherForecast }) => (
	<div className="home">
		<div className="title">
			Latest weather
		</div>
		<div className="current-weather-info">
			<WeatherTile weatherInfo={latestWeather} />
		</div>
		<ul className="forecast-tile-list">
			{weatherForecast.map(entry =>
			<li key={entry.timestamp} className="forecast-tile-list-item">
				<WeatherTile weatherInfo={entry} />
			</li>
			)}
		</ul>
	</div>
);

export default Home;
